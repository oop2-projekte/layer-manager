import java.awt.GridBagLayout;

import javax.swing.JPanel;

public class MainPanel extends JPanel {

	/**
	 * Run init code here (the view has valid dimensions at this point)
	 */
	public void init() {
		setLayout(new GridBagLayout());
	}
}
